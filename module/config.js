// Namespace Configuration Values
export const SWN = {};

// ASCII Artwork
SWN.ASCII = `_______________________________
______      ______ _____ _____ 
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__  
| | | / _ \\/\\ | | |   \\ \\  __| 
| |/ / (_>  < |/ //\\__/ / |___ 
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
SWN.abilities = {
  "str": "SWN.AbilityStr",
  "dex": "SWN.AbilityDex",
  "con": "SWN.AbilityCon",
  "int": "SWN.AbilityInt",
  "wis": "SWN.AbilityWis",
  "cha": "SWN.AbilityCha"
};

SWN.abilityAbbreviations = {
  "str": "SWN.AbilityStrAbbr",
  "dex": "SWN.AbilityDexAbbr",
  "con": "SWN.AbilityConAbbr",
  "int": "SWN.AbilityIntAbbr",
  "wis": "SWN.AbilityWisAbbr",
  "cha": "SWN.AbilityChaAbbr"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
SWN.alignments = {
  'lg': "SWN.AlignmentLG",
  'ng': "SWN.AlignmentNG",
  'cg': "SWN.AlignmentCG",
  'ln': "SWN.AlignmentLN",
  'tn': "SWN.AlignmentTN",
  'cn': "SWN.AlignmentCN",
  'le': "SWN.AlignmentLE",
  'ne': "SWN.AlignmentNE",
  'ce': "SWN.AlignmentCE"
};


SWN.weaponProficiencies = {
  "sim": "SWN.WeaponSimpleProficiency",
  "mar": "SWN.WeaponMartialProficiency"
};

SWN.toolProficiencies = {
  "art": "SWN.ToolArtisans",
  "disg": "SWN.ToolDisguiseKit",
  "forg": "SWN.ToolForgeryKit",
  "game": "SWN.ToolGamingSet",
  "herb": "SWN.ToolHerbalismKit",
  "music": "SWN.ToolMusicalInstrument",
  "navg": "SWN.ToolNavigators",
  "pois": "SWN.ToolPoisonersKit",
  "thief": "SWN.ToolThieves",
  "vehicle": "SWN.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
SWN.timePeriods = {
  "inst": "SWN.TimeInst",
  "turn": "SWN.TimeTurn",
  "round": "SWN.TimeRound",
  "minute": "SWN.TimeMinute",
  "hour": "SWN.TimeHour",
  "day": "SWN.TimeDay",
  "month": "SWN.TimeMonth",
  "year": "SWN.TimeYear",
  "perm": "SWN.TimePerm",
  "spec": "SWN.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
SWN.abilityActivationTypes = {
  "none": "SWN.None",
  "action": "SWN.Action",
  "bonus": "SWN.BonusAction",
  "reaction": "SWN.Reaction",
  "minute": SWN.timePeriods.minute,
  "hour": SWN.timePeriods.hour,
  "day": SWN.timePeriods.day,
  "special": SWN.timePeriods.spec,
  "legendary": "SWN.LegAct",
  "lair": "SWN.LairAct",
  "crew": "SWN.VehicleCrewAction"
};

/* -------------------------------------------- */


SWN.abilityConsumptionTypes = {
  "ammo": "SWN.ConsumeAmmunition",
  "attribute": "SWN.ConsumeAttribute",
  "material": "SWN.ConsumeMaterial",
  "charges": "SWN.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
SWN.actorSizes = {
  "tiny": "SWN.SizeTiny",
  "sm": "SWN.SizeSmall",
  "med": "SWN.SizeMedium",
  "lg": "SWN.SizeLarge",
  "huge": "SWN.SizeHuge",
  "grg": "SWN.SizeGargantuan"
};

SWN.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

// Vehicle frame types
SWN.frameTypes = {
  "atv": "SWN.FrameATVExplorer",
  "gravcar": "SWN.FrameGravcar",
  "gravflyer": "SWN.FrameGravflyer",
  "gravtank": "SWN.FrameGravtank",
  "groundcar": "SWN.FrameGroundcar",
  "helicopter": "SWN.FrameHelicopter",
  "hovercycle": "SWN.FrameHovercycle",
  "motorcycle":"SWN.FrameMotorcycle"
};

/* -------------------------------------------- */

/* -------------------------------------------- */

// Fuel Types
SWN.fuelTypes = {
  "biofuel": "SWN.FuelBiofuel",
  "energyCell": "SWN.FuelEnergyCell",
  "fossilFuel": "SWN.FuelFossilFuel"
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
SWN.itemActionTypes = {
  "mwak": "SWN.ActionMWAK",
  "rwak": "SWN.ActionRWAK",
  "msak": "SWN.ActionMSAK",
  "rsak": "SWN.ActionRSAK",
  "save": "SWN.ActionSave",
  "heal": "SWN.ActionHeal",
  "abil": "SWN.ActionAbil",
  "util": "SWN.ActionUtil",
  "other": "SWN.ActionOther"
};

/* -------------------------------------------- */

SWN.itemCapacityTypes = {
  "items": "SWN.ItemContainerCapacityItems",
  "weight": "SWN.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
SWN.limitedUsePeriods = {
  "sr": "SWN.ShortRest",
  "lr": "SWN.LongRest",
  "day": "SWN.Day",
  "charges": "SWN.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
SWN.equipmentTypes = {
  "light": "SWN.EquipmentLight",
  "medium": "SWN.EquipmentMedium",
  "heavy": "SWN.EquipmentHeavy",
  "bonus": "SWN.EquipmentBonus",
  "natural": "SWN.EquipmentNatural",
  "shield": "SWN.EquipmentShield",
  "clothing": "SWN.EquipmentClothing",
  "trinket": "SWN.EquipmentTrinket",
  "vehicle": "SWN.EquipmentVehicle"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
SWN.armorProficiencies = {
  "lgt": SWN.equipmentTypes.light,
  "med": SWN.equipmentTypes.medium,
  "hvy": SWN.equipmentTypes.heavy,
  "shl": "SWN.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
SWN.consumableTypes = {
  "ammo": "SWN.ConsumableAmmunition",
  "potion": "SWN.ConsumablePotion",
  "poison": "SWN.ConsumablePoison",
  "food": "SWN.ConsumableFood",
  "scroll": "SWN.ConsumableScroll",
  "wand": "SWN.ConsumableWand",
  "rod": "SWN.ConsumableRod",
  "trinket": "SWN.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
SWN.currencies = {
  "pp": "SWN.CurrencyPP",
  "gp": "SWN.CurrencyGP",
  "ep": "SWN.CurrencyEP",
  "sp": "SWN.CurrencySP",
  "cp": "SWN.CurrencyCP",
};


/**
 * Define the upwards-conversion rules for registered currency types
 * @type {{string, object}}
 */
SWN.currencyConversion = {
  cp: {into: "sp", each: 10},
  sp: {into: "ep", each: 5 },
  ep: {into: "gp", each: 2 },
  gp: {into: "pp", each: 10}
};

/* -------------------------------------------- */


// Damage Types
SWN.damageTypes = {
  "acid": "SWN.DamageAcid",
  "bludgeoning": "SWN.DamageBludgeoning",
  "cold": "SWN.DamageCold",
  "fire": "SWN.DamageFire",
  "force": "SWN.DamageForce",
  "lightning": "SWN.DamageLightning",
  "necrotic": "SWN.DamageNecrotic",
  "piercing": "SWN.DamagePiercing",
  "poison": "SWN.DamagePoison",
  "psychic": "SWN.DamagePsychic",
  "radiant": "SWN.DamageRadiant",
  "slashing": "SWN.DamageSlashing",
  "thunder": "SWN.DamageThunder"
};

// Damage Resistance Types
SWN.damageResistanceTypes = mergeObject(duplicate(SWN.damageTypes), {
  "physical": "SWN.DamagePhysical"
});


/* -------------------------------------------- */

SWN.distanceUnits = {
  "none": "SWN.None",
  "self": "SWN.DistSelf",
  "touch": "SWN.DistTouch",
  "ft": "SWN.DistFt",
  "mi": "SWN.DistMi",
  "spec": "SWN.Special",
  "any": "SWN.DistAny"
};

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
SWN.encumbrance = {
  currencyPerWeight: 50, 
  // stowed multiplier
  strMultiplier: 1,
  vehicleWeightMultiplier: 2000 // 2000 lbs in a ton
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
SWN.targetTypes = {
  "none": "SWN.None",
  "self": "SWN.TargetSelf",
  "creature": "SWN.TargetCreature",
  "ally": "SWN.TargetAlly",
  "enemy": "SWN.TargetEnemy",
  "object": "SWN.TargetObject",
  "space": "SWN.TargetSpace",
  "radius": "SWN.TargetRadius",
  "sphere": "SWN.TargetSphere",
  "cylinder": "SWN.TargetCylinder",
  "cone": "SWN.TargetCone",
  "square": "SWN.TargetSquare",
  "cube": "SWN.TargetCube",
  "line": "SWN.TargetLine",
  "wall": "SWN.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are SWN target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
SWN.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
SWN.healingTypes = {
  "healing": "SWN.Healing",
  "temphp": "SWN.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
SWN.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * Character senses options
 * @type {Object}
 */
SWN.senses = {
  "bs": "SWN.SenseBS",
  "dv": "SWN.SenseDV",
  "ts": "SWN.SenseTS",
  "tr": "SWN.SenseTR"
};


/* -------------------------------------------- */

/**
 * The set of skill which can be trained
 * @type {Object}
 */
SWN.skills = {
  "admin": "SWN.SkillAdmin",
  "connect": "SWN.SkillConnect",
  "exert": "SWN.SkillExert",
  "fix": "SWN.SkillFix",
  "gunnery": "SWN.SkillGunnery",
  "heal": "SWN.SkillHeal",
  "know": "SWN.SkillKnow",
  "lead": "SWN.SkillLead",
  "notice": "SWN.SkillNotice",
  "perform": "SWN.SkillPerform",
  "pilot": "SWN.SkillPilot",
  "program": "SWN.SkillProgram",
  "punch": "SWN.SkillPunch",
  "shoot": "SWN.SkillShoot",
  "sneak": "SWN.SkillSneak",
  "stab": "SWN.SkillStab",
  "survive": "SWN.SkillSurvive",
  "talk": "SWN.SkillTalk",
  "trade": "SWN.SkillTrade",
  "work": "SWN.SkillWork",
  "precognition": "SWN.SkillPrecognition",
  "metapsionics": "SWN.SkillMetapsionics",
  "telekinesis": "SWN.SkillTelekinesis",
  "telepathy": "SWN.SkillTelepathy",
  "teleportation": "SWN.SkillTeleportation"
};

/* -------------------------------------------- */
/**
 * Focus information
 */

 SWN.focusSkills = {
  "focusAlert": "SWN.FocusAlert",
  "focusArmsman": "SWN.FocusArmsman",
  "focusAssassin": "SWN.FocusAssassin",
  "focusAuthority": "SWN.FocusAuthority",
  "focusCloseCombat": "SWN.FocusCloseCombat",
  "focusConnected": "SWN.FocusConnected",
  "focusDieHard": "SWN.FocusDieHard",
  "focusDiplomat": "SWN.FocusDiplomat",
  "focusGunslinger": "SWN.FocusGunslinger",
  "focusHacker": "SWN.FocusHacker",
  "focusHealer": "SWN.FocusHealer",
  "focusHenchkeeper": "SWN.FocusHenchkeeper",
  "focusIronhide": "SWN.FocusIronhide",
  "focusPsychicTraining": "SWN.FocusPsychicTraining",
  "focusSavageFray": "SWN.FocusSavageFray",
  "focusShockingAssault": "SWN.FocusShockingAssault",
  "focusSniper": "SWN.FocusSniper",
  "focusSpecialist": "SWN.FocusSpecialist",
  "focusStarCaptain": "SWN.FocusStarCaptain",
  "focusStarfarer": "SWN.FocusStarfarer",
  "focusTinker": "SWN.FocusTinker",
  "focusUnarmedCombatant": "SWN.FocusUnarmedCombatant",
  "focusUniqueGift": "SWN.FocusUniqueGift",
  "focusWanderer": "SWN.FocusWanderer",
  "focusWildPsychicTalent": "SWN.FocusWildPsychicTalent"
 };

/* -------------------------------------------- */

/* -------------------------------------------- */

SWN.spellPreparationModes = {
  "always": "SWN.SpellPrepAlways",
  "atwill": "SWN.SpellPrepAtWill",
  "innate": "SWN.SpellPrepInnate",
  "pact": "SWN.PactMagic",
  "prepared": "SWN.SpellPrepPrepared"
};

SWN.spellUpcastModes = ["always", "pact", "prepared"];


SWN.spellProgression = {
  "none": "SWN.SpellNone",
  "full": "SWN.SpellProgFull",
  "half": "SWN.SpellProgHalf",
  "third": "SWN.SpellProgThird",
  "pact": "SWN.SpellProgPact",
  "artificer": "SWN.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
SWN.spellScalingModes = {
  "none": "SWN.SpellNone",
  "cantrip": "SWN.SpellCantrip",
  "level": "SWN.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
SWN.weaponTypes = {
  "simpleM": "SWN.WeaponSimpleM",
  "simpleR": "SWN.WeaponSimpleR",
  "martialM": "SWN.WeaponMartialM",
  "martialR": "SWN.WeaponMartialR",
  "natural": "SWN.WeaponNatural",
  "improv": "SWN.WeaponImprov",
  "siege": "SWN.WeaponSiege"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
SWN.weaponProperties = {
  "amm": "SWN.WeaponPropertiesAmm",
  "burst": "SWN.WeaponPropertiesBurst",
  "hvy": "SWN.WeaponPropertiesHvy",
  "fin": "SWN.WeaponPropertiesFin",
  "fir": "SWN.WeaponPropertiesFir",
  "foc": "SWN.WeaponPropertiesFoc",
  "lgt": "SWN.WeaponPropertiesLgt",
  "lod": "SWN.WeaponPropertiesLod",
  "rch": "SWN.WeaponPropertiesRch",
  "rel": "SWN.WeaponPropertiesRel",
  "ret": "SWN.WeaponPropertiesRet",
  "spc": "SWN.WeaponPropertiesSpc",
  "thr": "SWN.WeaponPropertiesThr",
  "two": "SWN.WeaponPropertiesTwo",
  "ver": "SWN.WeaponPropertiesVer"
};


// Spell Components
SWN.spellComponents = {
  "V": "SWN.ComponentVerbal",
  "S": "SWN.ComponentSomatic",
  "M": "SWN.ComponentMaterial"
};

// Spell Schools
SWN.spellSchools = {
  "abj": "SWN.SchoolAbj",
  "con": "SWN.SchoolCon",
  "div": "SWN.SchoolDiv",
  "enc": "SWN.SchoolEnc",
  "evo": "SWN.SchoolEvo",
  "ill": "SWN.SchoolIll",
  "nec": "SWN.SchoolNec",
  "trs": "SWN.SchoolTrs"
};

// Spell Levels
SWN.spellLevels = {
  0: "SWN.SpellLevel0",
  1: "SWN.SpellLevel1",
  2: "SWN.SpellLevel2",
  3: "SWN.SpellLevel3",
  4: "SWN.SpellLevel4",
  5: "SWN.SpellLevel5",
  6: "SWN.SpellLevel6",
  7: "SWN.SpellLevel7",
  8: "SWN.SpellLevel8",
  9: "SWN.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
SWN.spellScrollIds = {
  0: 'Compendium.dnd5e.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.dnd5e.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.dnd5e.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.dnd5e.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.dnd5e.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.dnd5e.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.dnd5e.items.tI3rWx4bxefNCexS',
  7: 'Compendium.dnd5e.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.dnd5e.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.dnd5e.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
SWN.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];

/* -------------------------------------------- */

// Polymorph options.
SWN.polymorphSettings = {
  keepPhysical: 'SWN.PolymorphKeepPhysical',
  keepMental: 'SWN.PolymorphKeepMental',
  keepSaves: 'SWN.PolymorphKeepSaves',
  keepSkills: 'SWN.PolymorphKeepSkills',
  mergeSaves: 'SWN.PolymorphMergeSaves',
  mergeSkills: 'SWN.PolymorphMergeSkills',
  keepClass: 'SWN.PolymorphKeepClass',
  keepFeats: 'SWN.PolymorphKeepFeats',
  keepSpells: 'SWN.PolymorphKeepSpells',
  keepItems: 'SWN.PolymorphKeepItems',
  keepBio: 'SWN.PolymorphKeepBio',
  keepVision: 'SWN.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
SWN.proficiencyLevels = {
  0: "SWN.NotProficient",
  1: "SWN.Proficient",
  0.5: "SWN.HalfProficient",
  2: "SWN.Expertise"
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
SWN.cover = {
  0: 'SWN.None',
  .5: 'SWN.CoverHalf',
  .75: 'SWN.CoverThreeQuarters',
  1: 'SWN.CoverTotal'
};

/* -------------------------------------------- */


// Condition Types
SWN.conditionTypes = {
  "blinded": "SWN.ConBlinded",
  "charmed": "SWN.ConCharmed",
  "deafened": "SWN.ConDeafened",
  "diseased": "SWN.ConDiseased",
  "exhaustion": "SWN.ConExhaustion",
  "frightened": "SWN.ConFrightened",
  "grappled": "SWN.ConGrappled",
  "incapacitated": "SWN.ConIncapacitated",
  "invisible": "SWN.ConInvisible",
  "paralyzed": "SWN.ConParalyzed",
  "petrified": "SWN.ConPetrified",
  "poisoned": "SWN.ConPoisoned",
  "prone": "SWN.ConProne",
  "restrained": "SWN.ConRestrained",
  "stunned": "SWN.ConStunned",
  "unconscious": "SWN.ConUnconscious"
};

// Languages
SWN.languages = {
  "arabic": "SWN.LanguagesArabic",
  "english": "SWN.LanguagesEnglish",
  "kannada": "SWN.LanguagesKannada",
  "mandarin": "SWN.LanguagesMandarin",
  "spanish": "SWN.LanguagesSpanish",
  "swahili": "SWN.LanguagesSwahili"
};

// Character Level XP Requirements
SWN.CHARACTER_EXP_LEVELS =  [
  0, 3, 6, 12, 18, 27, 39, 54, 72, 93]
;

// Challenge Rating XP Levels
SWN.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Configure Optional Character Flags
SWN.characterFlags = {
  "powerfulBuild": {
    name: "SWN.FlagsPowerfulBuild",
    hint: "SWN.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "savageAttacks": {
    name: "SWN.FlagsSavageAttacks",
    hint: "SWN.FlagsSavageAttacksHint",
    section: "Racial Traits",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "SWN.FlagsElvenAccuracy",
    hint: "SWN.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "SWN.FlagsHalflingLucky",
    hint: "SWN.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "SWN.FlagsInitiativeAdv",
    hint: "SWN.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "SWN.FlagsAlert",
    hint: "SWN.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "SWN.FlagsJOAT",
    hint: "SWN.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "SWN.FlagsObservant",
    hint: "SWN.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "reliableTalent": {
    name: "SWN.FlagsReliableTalent",
    hint: "SWN.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "SWN.FlagsRemarkableAthlete",
    hint: "SWN.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "SWN.FlagsCritThreshold",
    hint: "SWN.FlagsCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  }
};

// Configure allowed status flags
SWN.allowedActorFlags = [
  "isPolymorphed", "originalActor"
].concat(Object.keys(SWN.characterFlags));
