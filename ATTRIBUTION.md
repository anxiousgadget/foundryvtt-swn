 # Attribution and recognition
 - Kevin Crawford
    - Kevin created "Stars Without Number." You can find the free version on Drive Thru RPG: https://www.drivethrurpg.com/product/230009/Stars-Without-Number-Revised-Edition-Free-Version ; please consider purchasing the paid version: https://www.drivethrurpg.com/product/226996/Stars-Without-Number-Revised-Edition and the supporting materials

 - Atropos
    - This system is based off of the dnd5e system created by Atropos.

 - Smashicons
    - Smashicons made the icons used in the items compendium free for private and commercial use. https://www.flaticon.com/authors/smashicons