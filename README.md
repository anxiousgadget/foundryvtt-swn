# Foundry Virtual Tabletop - Stars Without Number System

This game system for [Foundry Virtual Tabletop](http://foundryvtt.com) provides character sheet and game system 
support for Stars Without Number, written by Kevin Crawford and available on Drive Thru RPG in both free and paid versions.

This system provides character sheet support for Actors and Items, mechanical support for dice and rules necessary to
play games of Stars Without Number (Free Edition). Content from the paid version, or additional material written by Crawford, are NOT supported in this system.

The software component of this system is distributed under the GNUv3 license.

## Installation Instructions

To install and use the SWN system for Foundry Virtual Tabletop, simply paste the following URL into the 
**Install System** dialog on the Setup menu of the application.

https://gitlab.com/anxiousgadget/foundryvtt-swn/raw/master/system.json

If you wish to manually install the system, you must clone or extract it into the ``Data/systems/foundryvtt-swn`` folder. You
may do this by cloning the repository.

## Icon & Token Specifications

Icons are 256x256, 72dpi, and in the .jpg format

Tokens are 400x400, and are in the .png format